from TimeTable import TimeTable
import shelve


def choosecmp(name1, name2):
    return name1.compare(name2)


def names():
    s = shelve.open("friends.dat")
    k = list(s.keys())
    return k


def setsched():
    s = shelve.open("friends.dat")
    k = list(s.keys())
    for i in range(len(k)):
        print(str(i + 1) + ' ' + s[k[i]].name)
    nam = input(""" type esc to back or
        Enter new name or choose number of saved names : """)

    if nam == 'esc':
        print(nam)
        return 'back'

    if nam.isdigit():

        try:
            nam = k[int(nam) - 1]
        except:
            print('out of range')

    if nam not in names():
        friend = TimeTable(nam)
        print('создается расписание.')

        while True:
            day = input('''choose day, enter its number (0-6): 
                        or type esc to back to main menu
                        ''')
            if day == 'esc':
                break
            else:
                try:
                    day = int(day)
                except:
                    print('день- это число')

            subjs = [subj for subj in input('enter day subjects : ').split()]
            if 'esc' in subjs:
                break
            friend.addd(day, subjs)
            s[friend.name] = friend

            print('')



    else:

        day = input('choose day, enter its number (0-6): ')
        if day == 'esc':
            return 'back'

        choice = input('изменить одну пару или все сразу? 0 or 1')
        if choice == 'esc':
            return 'back'

        if choice == '0':

            num = int(input('choose number of class')) - 1
            subj = input('enter your class')
            try:
                friend = s[nam]
                friend.add(int(day), num, subj)
                s[nam] = friend
            except:
                print('чёто не то ты ввёл')
                return ''


        elif choice == '1':

            subjs = [subj for subj in input('enter day subjects : ').split()]
            friend = s[nam]
            friend.addd(int(day), subjs)
            s[nam] = friend
        else:
            print('надо было вводить 0 или 1...')

    s.close()


def tocompare():
    print('кого сравнить? номер -> enter, number -> enter ')
    s = shelve.open("friends.dat")
    k = list(s.keys())
    for i in range(len(k)):
        print(str(i + 1) + ' ' + s[k[i]].name)
    try:
        choice1 = int(input())
        choice2 = int(input())
    except:
        return 'esc'
    try:
        choosecmp(s[k[choice1 - 1]], s[k[choice2 - 1]])
    except:
        print('возможно,вы не добавили расписания и сравнивать нечего :)')
    s.close()


def clearr():
    print('кого стереть? : ')
    s = shelve.open("friends.dat")
    k = list(s.keys())
    for i in range(len(k)):
        print(str(i + 1) + ' ' + s[k[i]].name)

    try:
        choice = int(input())
    except:
        print('вводить нужно число')
    try:
        del s[k[choice - 1]]
    except:
        print('друга под таким номером нет')
    s.close()


def look():
    print('кого посмотреть? : ')
    s = shelve.open("friends.dat")
    k = list(s.keys())

    for i in range(len(k)):
        print(str(i + 1) + ' ' + s[k[i]].name)
    choice = input()
    if choice == 'esc':
        return 'back'
    else:
        choice = int(choice)
    try:
        s[k[choice - 1]].toString()
    except:
        print('возможно,вы не добавили расписания друзей и смотреть нечего :)')

    s.close()


def copyy():
    s = shelve.open("friends.dat")
    k = list(s.keys())

    for i in range(len(k)):
        print(str(i + 1) + ' ' + s[k[i]].name)
    choice = input('''кому будем передавать чужое расписание? создайте
                новое или выберите из списка''')

    nam = input("""чьё расписание скопировать? """)


    if choice.isdigit():
        try:
            choice = k[int(choice) - 1]

        except:
            print('out of range')
    if choice not in k:
        friend = TimeTable(choice)
        friend.copy(s[k[int(nam)]])
        s[friend.name] = friend

    else:

        friend = s[choice]
        friend.copy(s[k[int(nam)]])
        s[choice] = friend

    s.close()
def changeName():
    s = shelve.open("friends.dat")
    k = list(s.keys())

    for i in range(len(k)):
        print(str(i + 1) + ' ' + s[k[i]].name)
    try:
        choice = int(input('выберите человека,чье имя хотите поменять'))
    except:
        print('цифру,дорогой')
        return ''
    try:
        friend = s[k[choice-1]]
        choice1 = input('new name : ')
        friend.name = choice1
        s[k[choice-1]] = friend
    except:
        return ''
    s.close()
def main():
    choice = None

    while choice != '0':

        print('''
            1 - Добавить расписание друга
            2 - сравнить 2 расписания на наличие общих окон 
            3 - посмотреть расписание
            4 - delete friend's schedule
            5 - copy someone's schedule to another
            6 - change friend's name
            0 - exit
            
            esc - back to main menu
            ''')
        choice = input()
        if choice == '1':
            setsched()

        elif choice == '2':
            tocompare()

        elif choice == '3':
            look()
        elif choice == '4':
            clearr()
        elif choice == '5':
            copyy()
        elif choice == '6':
            changeName()
        if choice == '0':
            print('by')
            input()


main()
