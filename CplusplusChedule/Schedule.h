#pragma once

#include <iostream>
#include <vector>

const int MAX_CLASSES = 5;


enum StudyType {
    CLASS, LECTURE, BREAK
};


class Schedule {
public:
    Schedule();
    ~Schedule();

    void fillWith(StudyType);
    void add(StudyType);

    void clear();

    const std::vector<StudyType>& get();
	
	friend std::ostream& operator<<(std::ostream&, const Schedule&);
private:
    std::vector<StudyType> sch;
};


