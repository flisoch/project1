#include <iostream>
#include <fstream>

#include <vector>
#include <map>
#include <string>
#include <algorithm>

#include "Schedule.h"
#include "TimeTable.h"

using std::string;
using std::vector;

const int MAX_LEN = 64;

vector<string>& split(const string& line, string sep = " .,:;") {
    vector<string> split;
    string word;

    for (auto chr : line) {
        if ( std::count(sep.begin(), sep.end(), chr)
                && !word.empty() ) {
            split.push_back(word);
            word.clear();
        } else {
            word += chr;
        }
    }
    if ( !word.empty() ) {
        split.push_back(word);
    }

    return split;
}



int main() {
	std::ifstream fin("data.base");

	map<string, TimeTable> group;

	while (!fin.eof()) {        
        string line;

        fin.getLine(line, MAX_LEN);

        auto v = split(line);

        TimeTable t;
        t.add(v[1], v.end());

		group[ v[0] ] = t;
	}
	
	string info =
            "1 - add friend\'s schedule\n"
            + "2 - compare two schedules for common breaks\n"
            + "3 - check schedule\n"
            + "4 - copy someone\'s schedule\n"
            + "0 - exit\n"
    ;

    std::cout << name << group[name];

	while (true) {
		std::cout << info << std::endl;
		
		int query;
		std::cin >> query;

		switch (query) {
        case 1:

        
        case 2:
            string name1;
            std::cin >> name1;
            string name2;
            std::cin >> name2;

            group[name1].compare(group[name2]);
            break;

		case 3:
			string name;
			std::cin >> name;

			std::cout << group[name];
			break;

        case 

		default:
			std::cout << "Parsing error.";
		}
    }


    fin.close();

    std::ofstream fout("data.base");


    // FILE OUTPUT

    for (const auto& [name, table] : group) {
        fout << name << ' ' << table << std::endl;
    }
    
    fout.close();

	return 0;
}
